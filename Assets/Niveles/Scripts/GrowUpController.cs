﻿using UnityEngine;
using System.Collections;

public class GrowUpController : MonoBehaviour {

	// movement config
	public float gravity = -25f;
	public float runSpeed = 8f;
	public float groundDamping = 20f; // how fast do we change direction? higher means faster
	public float inAirDamping = 5f;
	public float jumpHeight = 3f;
		
	[HideInInspector]
	private float normalizedHorizontalSpeed = 0;
	
	private CharacterController2D controller;
	private RaycastHit2D lastControllerColliderHit;
	private Vector3 velocity;
		
	// input
	public bool right;
	public bool left;

	private bool isReady = false;

	//se ejecuta antes que start
	void Awake(){
		controller = GetComponent<CharacterController2D>();
		
		// listen to some events for illustration purposes, es la forma de conectar el charcont2d mediante estas funciones
		controller.onControllerCollidedEvent += onControllerCollider;
		controller.onTriggerEnterEvent += onTriggerEnterEvent;
		controller.onTriggerExitEvent += onTriggerExitEvent;
	}

	#region Event Listeners
	void onControllerCollider(RaycastHit2D hit){
		if (controller.collisionState.left) {
			right = true;
			left = false;
		}
		else if (controller.collisionState.right) {
			right = false;
			left = true;
		}
	}
		
	void onTriggerEnterEvent(Collider2D col){
	
	}
		
	void onTriggerExitEvent(Collider2D col){

	}
	#endregion

	//ace que la seta salga acia arriba
	void Start () {
		iTween.MoveAdd (this.gameObject, iTween.Hash ("y",0.95f,
		                                              "time", 0.3f,
		                                              "easetype", "linear",
		                                              "oncomplete", "finishAnimationUp"));

	}

	void finishAnimationUp(){
		isReady = true;
	}


	//movimiento de la seta
	void FixedUpdate(){
		if (isReady) {

			// grab our current velocity to use as a base for all calculations
			velocity = controller.velocity;

			if (controller.isGrounded) {
				velocity.y = 0;
			}

			if (right) {
				normalizedHorizontalSpeed = 1;

				//cuando en un if solo hay una linea no hace falta poner las llaves
				if (transform.localScale.x < 0f)
					transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
			} else if (left) {
				normalizedHorizontalSpeed = -1;
				if (transform.localScale.x > 0f)
					transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
			} else {
				normalizedHorizontalSpeed = 0;
			}

			//caida desde altura
			if (velocity.y < -1) {

			}		

			// apply horizontal speed smoothing it
			var smoothedMovementFactor = controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
			velocity.x = Mathf.Lerp (velocity.x, normalizedHorizontalSpeed * runSpeed, Time.fixedDeltaTime * smoothedMovementFactor);

			// apply gravity before moving
			velocity.y += gravity * Time.fixedDeltaTime;

			controller.move (velocity * Time.fixedDeltaTime);
		}
	}
}
