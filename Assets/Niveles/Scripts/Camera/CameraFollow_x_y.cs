using UnityEngine;
using System.Collections;

public class CameraFollow_x_y : MonoBehaviour
{
	private GameObject target;
	public bool oneDirectionOnly;
	public float minX = -5;
	public float maxX = 5;
	public float minY = -5;
	public float maxY = 5;

	public float smooth = 0.0f;

	void Update () {
		target = GameObject.FindGameObjectWithTag("Player");

		if(!target){
			target = GameObject.FindGameObjectWithTag("NaveInicio");
		}

		float deltaX = target.transform.position.x - transform.position.x;
		float deltaY = target.transform.position.y - transform.position.y;

		if (!oneDirectionOnly || deltaX > 0.0f || deltaY > 0.0f) {
			transform.Translate(deltaX * Time.deltaTime * smooth, deltaY * Time.deltaTime * smooth, 0.0f);
		}

		if (transform.position.x < minX) transform.Translate(minX - transform.position.x, 0, 0);
		if (transform.position.x > maxX) transform.Translate(maxX - transform.position.x , 0, 0);

		if (transform.position.y < minY) transform.Translate(0, minY - transform.position.y, 0);
		if (transform.position.y > maxY) transform.Translate(0, maxY - transform.position.y , 0);
	}

	//esta funcion es llamada desde cameraupdown
	void desbloquearCamara(float min){
		minY = min;
	}
}