﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class RpwnClass{

	public GameObject[] respawns;

	void Start () {
		for (int i = 0; i< respawns.Length; i++) {
			respawns[i] = GameObject.FindGameObjectWithTag("RpwnObjects");	
		}
	}
}
