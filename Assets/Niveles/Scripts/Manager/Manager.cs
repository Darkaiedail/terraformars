﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Manager : MonoBehaviour {

	//personaje ppal
	public GameObject pj;
	public Transform respawn;
	public Transform salida;
	private GameObject personaje;
	public GameObject puertaEntrada;
	public GameObject puertaSalida;
	public GameObject ship;

	//vidas
	private int vidas = 5;
	public Text campoVidas;

	//bonus
	private int bonus = 0;
	public Text campoBonus;
	public int totalBonus;

	//para controlar que coge solo un item
	private bool finishTakeItem = false;

	//personajes secundarios
	public GameObject capDavis;
	public GameObject Nanao;
	Vector2 pos = new Vector2(-129.5f,0.1f);
	Vector2 posN = new Vector2(-127.3f,0.5f);

	void Start(){
		StartCoroutine (iniciaJuego());
	}

	IEnumerator iniciaJuego(){
		yield return new WaitForSeconds (2.7f);
		personaje = Instantiate (pj, respawn.position, Quaternion.identity) as GameObject;
		Instantiate (capDavis,pos,Quaternion.identity);
		Instantiate (Nanao,posN,Quaternion.identity);
	}

	//----------------------------------------------------------------------Suma/resta de bonus y vidas
	void Update(){
		finishTakeItem = false;

		if (vidas == 0) {
			Application.LoadLevel(5);	
		}
	}

	//esta funcion es llamada desde physicsPlayerTester y landminecontroller
	void restaVidas(){
		if (!finishTakeItem) {
			finishTakeItem = true;
			vidas -= 1;
			string vidasStr = vidas.ToString ("00");
			campoVidas.text = vidasStr;
		}
	}

	//esta funcion es llamada desde physicsPlayerTester
	void sumaVidas(){
		if (!finishTakeItem) {
			finishTakeItem = true;
			vidas += 1;
			string vidasStr = vidas.ToString ("00");
			campoVidas.text = vidasStr;
		}
	}

	//esta funcion es llamada desde physicsPlayerTester
	void sumaBonus(){
		if (!finishTakeItem) {
			finishTakeItem = true;
			bonus += 1;
			string bonusStr = bonus.ToString ("00" + "/" + totalBonus);
			campoBonus.text = bonusStr;
		}
	}
	//---------------------------------------------------------------------- Fin suma/resta de bonus y vidas




	//----------------------------------------------------------------------Eventos de interaccion con el entorno
	//esta funcion es llamada desde physplaytester para abrir las puertas del camino
	void entrarPuerta(){
		StartCoroutine (instanciarEnSalida ());
		puertaEntrada.SendMessage("abrir");//funcion de puertacont
	}

	IEnumerator instanciarEnSalida(){
		yield return new WaitForSeconds (0.1f);
		puertaSalida.SendMessage("cerrar");//funcion de puertacont
		Destroy (personaje.gameObject);
		personaje = Instantiate (pj, salida.position, Quaternion.identity) as GameObject;
	}

	//esta funcion es llamada desde physplaytester para abrir la puerta de la nave final
	void open(){
		ship.SendMessage ("open");//funcion de shipcont
	}

	//checkpoint
	void checkPoint(){
		respawn.transform.position = personaje.gameObject.transform.position;
	}
	void respawnPj(float time){
		Destroy (personaje.gameObject, time);
		personaje = Instantiate (pj, respawn.position, Quaternion.identity) as GameObject;
	}

	//respawn de elementos del escenario

	//------------------------------------------------------------------Fin eventos de interaccion con el entorno
}