﻿using UnityEngine;
using System.Collections;

public class NaveController : MonoBehaviour {
		
	// movement config
	public float gravity = -25f;
	public float runSpeed = 8f;
	public float groundDamping = 20f; // how fast do we change direction? higher means faster
	public float inAirDamping = 5f;
	public float jumpHeight = 3f;
	
	[HideInInspector]
	private float normalizedHorizontalSpeed = 0;
	
	private CharacterController2D controller;
	private Animator animator;
	private RaycastHit2D lastControllerColliderHit;
	private Vector3 velocity;
	private GameObject manager;
	
	// input
	private bool right;
	private bool left;
	private bool up;

	//se ejecuta antes que start
	void Awake(){
		animator = GetComponent<Animator>();
		controller = GetComponent<CharacterController2D>();
		manager = GameObject.FindGameObjectWithTag("Manager");
		
		// listen to some events for illustration purposes, es la forma de conectar el charcont2d mediante estas funciones
		controller.onControllerCollidedEvent += onControllerCollider;
		controller.onTriggerEnterEvent += onTriggerEnterEvent;
		controller.onTriggerExitEvent += onTriggerExitEvent;
	}
	
	#region Event Listeners
	void onControllerCollider( RaycastHit2D hit ){
		
		//para que el pj se muera
		if (hit.collider.gameObject.CompareTag ("Enemy")) {
			Destroy(this.gameObject);				
			manager.SendMessage("restaVidas");
		}
	}
	
	void onTriggerEnterEvent( Collider2D col ){
		//Debug.Log( "onTriggerEnterEvent: " + col.gameObject.name );
	}
	
	void onTriggerExitEvent( Collider2D col ){
		//Debug.Log( "onTriggerExitEvent: " + col.gameObject.name );
	}
	#endregion
	
	//cojemos los cursores para que se mueva el pj
	// the Update loop only gathers input. Actual movement is handled in FixedUpdate because we are using the Physics system for movement
	void Update(){
		// a minor bit of trickery here. FixedUpdate sets up to false so to ensure we never miss any jump presses we leave up
		// set to true if it was true the previous frame
		up = up || Input.GetKeyDown( KeyCode.UpArrow );
		right = Input.GetKey( KeyCode.RightArrow );
		left = Input.GetKey( KeyCode.LeftArrow );
	}
	
	void FixedUpdate(){
		// grab our current velocity to use as a base for all calculations
		velocity = controller.velocity;
		
		if (controller.isGrounded) {
			velocity.y = 0;
		}
		
		//movimientos
		if (right) {
			normalizedHorizontalSpeed = 1;
			
			if (transform.localScale.x < 0f)
				transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
			
			if (controller.isGrounded){
				animator.Play (Animator.StringToHash ("Run"));	
			}
		} 
		else if (left) {
			normalizedHorizontalSpeed = -1;
			
			if (transform.localScale.x > 0f)
				transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
			
			if (controller.isGrounded){
					animator.Play (Animator.StringToHash ("Run"));	
			}					
		} 
		else {
			normalizedHorizontalSpeed = 0;
			
			if (controller.isGrounded) {
				animator.Play (Animator.StringToHash ("Idle"));
			}
		}
		
		//caida desde altura
		if (velocity.y < -1) {
			animator.Play (Animator.StringToHash ("Run"));
		}
		
		// we can only jump whilst grounded
		if (controller.isGrounded && up) {
			velocity.y = Mathf.Sqrt (2f * jumpHeight * -gravity);
			animator.Play (Animator.StringToHash ("Run"));
		}		
		
		// apply horizontal speed smoothing it
		var smoothedMovementFactor = controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
		velocity.x = Mathf.Lerp (velocity.x, normalizedHorizontalSpeed * runSpeed, Time.fixedDeltaTime * smoothedMovementFactor);
		
		// apply gravity before moving
		velocity.y += gravity * Time.fixedDeltaTime;
		
		controller.move (velocity * Time.fixedDeltaTime);
		
		// reset input
		up = false;
	}
}

