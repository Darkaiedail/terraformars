﻿using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour {

	private Animator animator;
	public bool inicioFase = false;

	//sonidos
	public AudioSource aterrizaje;
	public AudioSource aperturaPuerta;
	
	void Start () {
		animator = GetComponent<Animator>();

		if (inicioFase) {
			aterrizaje.Play();

			iTween.MoveTo (this.gameObject, iTween.Hash ("y", 0.5f,
			                                             "time", 1.5f,
			                                             "easetype","linear",
			                                             "oncomplete","land"));
		}
	}

	void land(){
//		aperturaPuerta.Play ();
		animator.Play(Animator.StringToHash("Land"));
	}

	//esta funcion es llamada desde manager
	void open(){
		gameObject.rigidbody2D.isKinematic = true;
		gameObject.collider2D.enabled = false;

		aperturaPuerta.Play ();
		animator.Play(Animator.StringToHash ("Open"));
		StartCoroutine (finNivel());
	}

	IEnumerator finNivel(){
		yield return new WaitForSeconds (0.7f);
		Application.LoadLevel (6);
	}
}
