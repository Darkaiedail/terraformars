﻿using UnityEngine;
using System.Collections;

public class ShokichiTouchController : MonoBehaviour {
	
	
	// movement config
	public float gravity = -25f;
	public float runSpeed = 8f;
	public float groundDamping = 20f; // how fast do we change direction? higher means faster
	public float inAirDamping = 5f;
	public float jumpHeight = 3f;
	
	
	[HideInInspector]
	private float normalizedHorizontalSpeed = 0;
	
	private CharacterController2D controller;
	private Animator animator;
	private RaycastHit2D lastControllerColliderHit;
	private Vector3 velocity;
	private GameObject manager;
	
	
	// input
	private bool right;
	private bool left;
	private bool up;
	private bool down;
	private bool slide;
	private bool punch;
	private bool jump = false;

	//estado inicial = 0, estado super = 1,estado pistola = 2, estado muerto = 3
	private int state = 0;

	//fuerza de salto
	public Vector2 salto = new Vector2 (70.0f,70.0f);
		
	private GameObject camara;

	//se ejecuta antes que start
	void Awake(){
				
		animator = GetComponent<Animator>();
		controller = GetComponent<CharacterController2D>();
		manager = GameObject.FindGameObjectWithTag("Manager");
		
		// listen to some events for illustration purposes, es la forma de conectar el charcont2d mediante estas funciones
		controller.onControllerCollidedEvent += onControllerCollider;
		controller.onTriggerEnterEvent += onTriggerEnterEvent;
		controller.onTriggerExitEvent += onTriggerExitEvent;
	}

	void Start(){
		camara = GameObject.FindGameObjectWithTag ("MainCamera");
	}
	
	#region Event Listeners
	void onControllerCollider( RaycastHit2D hit ){

		if (state == 0) {
			//para que el pj se muera
			if (hit.collider.gameObject.CompareTag ("Enemy") && !punch && !slide) {
				state = 3;
				animator.Play(Animator.StringToHash ("Die"));
				manager.SendMessage("restaVidas");
			}
			//para matar a los enemigos
			if (hit.collider.gameObject.CompareTag ("Enemy") && controller.collisionState.right && punch ||
			    hit.collider.gameObject.CompareTag ("Enemy") && controller.collisionState.left && punch) {
				Destroy(hit.collider.gameObject,0.15f);
			}
			
			if (hit.collider.gameObject.CompareTag ("Enemy") && controller.collisionState.right && slide ||
			    hit.collider.gameObject.CompareTag ("Enemy") && controller.collisionState.left && slide) {
				Destroy(hit.collider.gameObject);
			}	
		}

		if (state == 1) {
			//para matar a los enemigos
			if (hit.collider.gameObject.CompareTag ("Enemy")) {
				Destroy(hit.collider.gameObject);
			}	
		}

		//para coger items
		if (hit.collider.gameObject.CompareTag ("Life")) {
			Destroy(hit.collider.gameObject);
			manager.SendMessage("sumaVidas");
		}
		if (hit.collider.gameObject.CompareTag ("Box") && controller.collisionState.right) {
			hit.collider.gameObject.collider2D.enabled = false;
			hit.collider.gameObject.SendMessage("openBox");
			hit.collider.gameObject.SendMessage("instanciaPistola");
			Destroy(hit.collider.gameObject,0.3f);			
		}
		if (hit.collider.gameObject.CompareTag ("Pistol") && controller.collisionState.right ||
		    hit.collider.gameObject.CompareTag ("Pistol") && controller.collisionState.left) {
			//state = 2;
			Destroy(hit.collider.gameObject);
		}
		if (hit.collider.gameObject.CompareTag ("Bonus")) {
			Destroy(hit.collider.gameObject);
			manager.SendMessage("sumaBonus");
		}
		if (hit.collider.gameObject.CompareTag ("Super")) {
			state = 1;
			Destroy(hit.collider.gameObject);
			StartCoroutine("normalState");
		}

		//para saltar sobre paredes
		if (hit.collider.gameObject.CompareTag ("JumpingWall") && up && controller.collisionState.left) {
			controller.SendMessage ("fisicas");
			this.rigidbody2D.isKinematic = false;
			salto.x *= -1;
			this.rigidbody2D.AddForce (salto);
			animator.Play(Animator.StringToHash ("Jump"));
			StartCoroutine("reiniciaFisicas");
		}
		if (hit.collider.gameObject.CompareTag ("JumpingWall") && up && controller.collisionState.right) {
			controller.SendMessage ("fisicas");
			this.rigidbody2D.isKinematic = false;
			salto.x *= 1;
			this.rigidbody2D.AddForce (salto);
			animator.Play(Animator.StringToHash ("Jump"));
			StartCoroutine("reiniciaFisicas");
		}

		//para que la camara te siga hacia abajo
		if (hit.collider.gameObject.CompareTag ("DownBlock") && controller.collisionState.below) {
			camara.SendMessage("desbloquearCamara");	
		}
		if (hit.collider.gameObject.CompareTag ("UpBlock") && controller.collisionState.below) {
			camara.SendMessage("bloqueaCamara");	
		}
		
		//Puertas
		if (hit.collider.gameObject.CompareTag ("PuertaEntrada") && controller.collisionState.right) {
			manager.SendMessage("entrarPuerta");	
		}
	}

	void onTriggerEnterEvent( Collider2D col ){
		//Debug.Log( "onTriggerEnterEvent: " + col.gameObject.name );
	}
	
	void onTriggerExitEvent( Collider2D col ){
		//Debug.Log( "onTriggerExitEvent: " + col.gameObject.name );
	}
	#endregion
	
	IEnumerator reiniciaFisicas(){
		yield return new WaitForSeconds (0.02f);
		controller.SendMessage ("normalizaFisicas");
		this.rigidbody2D.isKinematic = true;
	}

	IEnumerator normalState(){
		yield return new WaitForSeconds (10.0f);
		state = 0;
		runSpeed = 8;
	}

	void FixedUpdate(){
		// grab our current velocity to use as a base for all calculations
		velocity = controller.velocity;

		//cambio de velocidad en estado SUPER
		if (state == 1) {
			runSpeed = 15;	
		}

		if (controller.isGrounded) {
			velocity.y = 0;
			SendMessage("restituirTotalHorizontalRays");
		}
		
		//movimientos
		if (right) {
			normalizedHorizontalSpeed = 1;
			
			if (transform.localScale.x < 0f)
				transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
			
			if (controller.isGrounded){
				if (state == 0) {
					animator.Play (Animator.StringToHash ("Run"));
				}
				if (state == 1) {
					animator.Play (Animator.StringToHash ("SuperRun"));
				}
			}
		} 
		else if (left) {
			normalizedHorizontalSpeed = -1;
			
			if (transform.localScale.x > 0f)
				transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
			
			if (controller.isGrounded){
				if (state == 0) {
					animator.Play (Animator.StringToHash ("Run"));
				}
				if (state == 1) {
					animator.Play (Animator.StringToHash ("SuperRun"));
				}
			}
		} 
		else if (down) {
			SendMessage("disminuirTotalHorizontalRays");
			if (state == 0) {
				animator.SetBool ("Crouch", true);
			}
			if (state == 1) {
				animator.SetBool ("SuperCrouch", true);
			}
		} 
		else if (slide) {
			if (controller.isGrounded) {
				if (transform.localScale.x > 0f){
					SendMessage("disminuirTotalHorizontalRays");
					this.rigidbody2D.isKinematic = false;
					Vector2 salto = new Vector2 (150.0f,0);
					this.rigidbody2D.AddForce (salto);

					if(state == 0){
						animator.Play (Animator.StringToHash ("Slide"));
					}
					if(state == 1){
						animator.Play (Animator.StringToHash ("SuperSlide"));
					}

					StartCoroutine("reiniciaFisicas");
				}
				if (transform.localScale.x < 0f){
					SendMessage("disminuirTotalHorizontalRays");
					this.rigidbody2D.isKinematic = false;
					Vector2 salto = new Vector2 (-150.0f,0);
					this.rigidbody2D.AddForce (salto);

					if(state == 0){
						animator.Play (Animator.StringToHash ("Slide"));
					}
					if(state == 1){
						animator.Play (Animator.StringToHash ("SuperSlide"));
					}

					StartCoroutine("reiniciaFisicas");
				}
			}
		} 
		else if (punch) {
			if (controller.isGrounded) {
				if(state == 0){
					animator.Play (Animator.StringToHash ("Punch"));
				}
			}
		}
		else {
			normalizedHorizontalSpeed = 0;
			
			if (controller.isGrounded) {
				animator.SetBool ("Crouch", false);
				animator.SetBool("SuperCrouch",false);
				if(state == 0){
					animator.Play (Animator.StringToHash ("Idle"));
				}
				if(state == 1){
					animator.Play (Animator.StringToHash ("SuperIdle"));
				}
			}
		}
		
		//caida desde altura
		if (velocity.y < -1) {
			if(state == 0){
				animator.Play (Animator.StringToHash ("Fall"));
			}
			if(state == 1){
				animator.Play (Animator.StringToHash ("SuperFall"));
			}
		}
		
		// we can only jump whilst grounded
		if (controller.isGrounded && jump) {
			velocity.y = Mathf.Sqrt (2f * jumpHeight * -gravity);
			if(state == 0){
				animator.Play (Animator.StringToHash ("Jump"));
			}
			if(state == 1){
				animator.Play (Animator.StringToHash ("SuperJump"));
			}
		}		
		
		// apply horizontal speed smoothing it
		var smoothedMovementFactor = controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
		velocity.x = Mathf.Lerp (velocity.x, normalizedHorizontalSpeed * runSpeed, Time.fixedDeltaTime * smoothedMovementFactor);
		
		// apply gravity before moving
		velocity.y += gravity * Time.fixedDeltaTime;
		
		controller.move (velocity * Time.fixedDeltaTime);
		
		// reset input
		up = false;
		jump = false;
		right = false;
		left = false;
		down = false;
		slide = false;
		punch = false;
	}

	//ciclo vida gameobject
	void OnEnable(){
		EasyButton.On_ButtonDown += HandleOn_ButtonDown;
		EasyButton.On_ButtonPress += HandleOn_ButtonPress;
		EasyJoystick.On_JoystickMove += HandleOn_JoystickMove;//estamos aciendo un puente entre el joystick y el script
	}

	void HandleOn_ButtonDown (string buttonName){
		if (buttonName.Equals ("Restart")) {
			Application.LoadLevel(0);
		}
	}

	void HandleOn_ButtonPress (string buttonName){
		if (buttonName.Equals ("Punch")) {
			punch = true;
		}
		if (buttonName.Equals ("Slide")) {
			slide = true;
		}
	}

	void HandleOn_JoystickMove (MovingJoystick move){
		if (move.joystickValue.x > 0.5) {
			right = true;		
		} 
		if (move.joystickValue.x < -0.5) {
			left = true;		
		} 
		if (move.joystickValue.y < -0.5) {
			down = true;
		}
		if (move.joystickValue.y > 0.5) {
			jump = true;
		}
	}
}
