﻿using UnityEngine;
using System.Collections;

public class PhysicsPlayerTester : MonoBehaviour{

	// movement config
	public float gravity = -25f;
	public float runSpeed = 8f;
	public float groundDamping = 20f; // how fast do we change direction? higher means faster
	public float inAirDamping = 5f;
	public float jumpHeight = 3f;

	[HideInInspector]
	private float normalizedHorizontalSpeed = 0;

	private CharacterController2D controller;
	private Animator animator;
	private RaycastHit2D lastControllerColliderHit;
	private Vector3 velocity;
	private GameObject manager;
			
	// input
	private bool right;
	private bool left;
	private bool up;
	private bool down;
	private bool slide;
	private bool punch;

	//estado inicial = 0, estado super = 1,estado pistola = 2, estado muerto = 3
	private int state = 0;

	//fuerza de salto
	public Vector2 salto = new Vector2 (70.0f,70.0f);

	//collider pj
	public Collider2D circleCollider;

	//fuerza de deslizamiento
	private float fuerzaSlide = 300.0f;

	//joystick
	public static bool joy = false;

	//enemigo pistola
	public GameObject[] enemy;

	//sonidos
	public AudioSource jumpSound;
	public AudioSource runSound;
	public AudioSource slideSound;
	public AudioSource punchSound;
	public AudioSource bonusSound;
	public AudioSource superSound;
	public AudioSource lifeSound;
	public AudioSource damageSound;
	public AudioSource boxSound;

	//se ejecuta antes que start
	void Awake(){
		animator = GetComponent<Animator>();
		controller = GetComponent<CharacterController2D>();
		manager = GameObject.FindGameObjectWithTag("Manager");

		// listen to some events for illustration purposes, es la forma de conectar el charcont2d mediante estas funciones
		controller.onControllerCollidedEvent += onControllerCollider;
		controller.onTriggerEnterEvent += onTriggerEnterEvent;
		controller.onTriggerExitEvent += onTriggerExitEvent;
	}

	void Start(){
		for (int i=0; i<enemy.Length; i++) {
			enemy[i] = GameObject.FindGameObjectWithTag ("PistolEnemy");
		}
	}

	#region Event Listeners
	void onControllerCollider( RaycastHit2D hit ){

		//estados normal y pistol
		if (state == 0 || state == 2) {
			//para que el pj se muera
			if (hit.collider.gameObject.CompareTag ("Enemy") && !punch && !slide ||
			    hit.collider.gameObject.CompareTag ("FlyEnemy") && controller.collisionState.right ||
			    hit.collider.gameObject.CompareTag ("FlyEnemy") && controller.collisionState.left ||
			    hit.collider.gameObject.CompareTag ("PistolEnemy") && !punch && !slide){
				damageSound.Play();
				state = 3;//estado muerto
				animator.Play(Animator.StringToHash ("Die"));
				manager.SendMessage("restaVidas");
				manager.SendMessage("respawnPj",1.3f);
			}

			//para matar a los enemigos
			if (hit.collider.gameObject.CompareTag ("Enemy") && controller.collisionState.right && slide ||
			    hit.collider.gameObject.CompareTag ("Enemy") && controller.collisionState.left && slide) {
				Destroy(hit.collider.gameObject);
			}

			//para los enemigos voladores
			if(hit.collider.gameObject.CompareTag ("FlyEnemy") && controller.collisionState.below) {
				Destroy(hit.collider.gameObject);
			}

			if (hit.collider.gameObject.CompareTag ("PistolEnemy") && controller.collisionState.right && slide ||
			    hit.collider.gameObject.CompareTag ("PistolEnemy") && controller.collisionState.left && slide) {
				Destroy(hit.collider.gameObject);
			}
		}

		//estado normal
		if (state == 0) {
			//para matar a los enemigos
			if (hit.collider.gameObject.CompareTag ("Enemy") && controller.collisionState.right && punch ||
			    hit.collider.gameObject.CompareTag ("Enemy") && controller.collisionState.left && punch) {
				Destroy(hit.collider.gameObject,0.15f);
			}

			if (hit.collider.gameObject.CompareTag ("PistolEnemy") && controller.collisionState.right && punch ||
			    hit.collider.gameObject.CompareTag ("PistolEnemy") && controller.collisionState.left && punch) {
				Destroy(hit.collider.gameObject,0.15f);
			}
		}

		//estado super
		if (state == 1) {
			//para matar a los enemigos
			if (hit.collider.gameObject.CompareTag ("Enemy")) {
				Destroy(hit.collider.gameObject);
			}

			//para los enemigos voladores
			if(hit.collider.gameObject.CompareTag ("FlyEnemy")) {
				Destroy(hit.collider.gameObject);
			}

			//para matar a los enemigos
			if (hit.collider.gameObject.CompareTag ("PistolEnemy")) {
				Destroy(hit.collider.gameObject);
			}
		}

		//para que la camara te siga hacia abajo/arriba
		if (hit.collider.gameObject.CompareTag ("Detector") && controller.collisionState.right) {
			hit.collider.SendMessage("minCamaraDcha");//func de cameraUpDown
		}
		if (hit.collider.gameObject.CompareTag ("Detector") && controller.collisionState.left) {
			hit.collider.SendMessage("minCamaraIzda");//func de cameraUpDown
		}

		//para coger items
		if (hit.collider.gameObject.CompareTag ("Life")) {
			lifeSound.Play();
			Destroy(hit.collider.gameObject);
			manager.SendMessage("sumaVidas");
		}
		if (hit.collider.gameObject.CompareTag ("Box") && controller.collisionState.right) {
			hit.collider.gameObject.collider2D.enabled = false;
			hit.collider.gameObject.SendMessage("openBox");
			hit.collider.gameObject.SendMessage("instanciaPistola");
			Destroy(hit.collider.gameObject,0.3f);			
		}
		if (hit.collider.gameObject.CompareTag ("Pistol") && controller.collisionState.right ||
		    hit.collider.gameObject.CompareTag ("Pistol") && controller.collisionState.left) {
			state = 2;
			Destroy(hit.collider.gameObject);
			StartCoroutine("normalState");
		}
		if (hit.collider.gameObject.CompareTag ("Bonus")) {
			bonusSound.Play();
			Destroy(hit.collider.gameObject);
			manager.SendMessage("sumaBonus");
		}
		if (hit.collider.gameObject.CompareTag ("Super")) {
			superSound.Play();
			state = 1;
			Destroy(hit.collider.gameObject);
			for (int i=0; i < enemy.Length; i++){
				enemy[i].BroadcastMessage("enemySuperState");
			}
			StartCoroutine("normalState");
		}

		//para trepar por las paredes
		if (hit.collider.gameObject.CompareTag ("JumpingWall") && controller.collisionState.left) {
			controller.SendMessage ("fisicas");
			this.rigidbody2D.isKinematic = false;
			salto.x *= -1;
			this.rigidbody2D.AddForce (salto);
			animator.Play(Animator.StringToHash ("Jump"));
			StartCoroutine("reiniciaFisicas");
		}
		if (hit.collider.gameObject.CompareTag ("JumpingWall") && controller.collisionState.right) {
			controller.SendMessage ("fisicas");
			this.rigidbody2D.isKinematic = false;
			salto.x *= 1;
			this.rigidbody2D.AddForce (salto);
			animator.Play(Animator.StringToHash ("Jump"));
			StartCoroutine("reiniciaFisicas");
		}

		//Puertas
		if (hit.collider.gameObject.CompareTag ("PuertaEntrada") && controller.collisionState.right ||
		    hit.collider.gameObject.CompareTag ("PuertaEntrada") && controller.collisionState.left) {
			manager.SendMessage("entrarPuerta");//funcion de manager	
		}

		//para subir y bajar de plataformas
		if (hit.collider.gameObject.CompareTag ("Plataforma") && controller.collisionState.below) {
			this.transform.parent = hit.collider.transform;
			circleCollider.enabled = false;
		} else {
			this.transform.parent = null;
			circleCollider.enabled = true;
		}

		//para mover cajas
		if(hit.collider.gameObject.CompareTag("MovilBox")&& controller.collisionState.right){
			//boxSound.Play();
			hit.collider.gameObject.rigidbody2D.AddForce(new Vector2(6.0f,0.0f));
		}
		if(hit.collider.gameObject.CompareTag ("MovilBox") && controller.collisionState.left){
			//boxSound.Play();
			hit.collider.gameObject.rigidbody2D.AddForce(new Vector2(-6.0f,0.0f));
		}

		//Bloques que caen
		if (hit.collider.gameObject.CompareTag ("FallingBlockDetector")) {
			hit.collider.gameObject.SendMessage("fallBlocks");
			hit.collider.gameObject.collider2D.enabled = false;
		}

		//para que se deslice mas lejos cuando pasa por debajo de plataformas
		if (hit.collider.gameObject.CompareTag ("AS") && controller.collisionState.right) {
			hit.collider.gameObject.SendMessage("addSlideForce");
			hit.collider.gameObject.collider2D.enabled = false;
			fuerzaSlide = 800.0f;
		}
		if (hit.collider.gameObject.CompareTag ("AS") && controller.collisionState.left) {
			hit.collider.gameObject.SendMessage("addSlideForce");
			hit.collider.gameObject.collider2D.enabled = false;
			fuerzaSlide = 300.0f;
		}
		if (hit.collider.gameObject.CompareTag ("NS") && controller.collisionState.right) {
			hit.collider.gameObject.SendMessage("addSlideForce");
			hit.collider.gameObject.collider2D.enabled = false;
			fuerzaSlide = 300.0f;
		}
		if (hit.collider.gameObject.CompareTag ("NS") && controller.collisionState.left) {
			hit.collider.gameObject.SendMessage("addSlideForce");
			hit.collider.gameObject.collider2D.enabled = false;
			fuerzaSlide = 800.0f;
		}

		//checkpoint
		if (hit.collider.gameObject.CompareTag ("CheckPoint") && controller.collisionState.below) {
			manager.SendMessage("checkPoint");
			hit.collider.gameObject.SendMessage("checkPoint");
			hit.collider.gameObject.collider2D.enabled = false;
		}

		//para morir al caer al vacio
		if (hit.collider.gameObject.CompareTag ("PlanoCollider")) {
			manager.SendMessage("respawnPj",0.0f);
			manager.SendMessage("restaVidas");
		}

		//final de fase
		if (hit.collider.gameObject.CompareTag ("Nave") && controller.collisionState.right) {
			manager.SendMessage("open");//func de manager
			//Application.Loadlevel(3);
		}
	}

	void onTriggerEnterEvent( Collider2D col ){
		//Debug.Log( "onTriggerEnterEvent: " + col.gameObject.name );
	}

	void onTriggerStayEvent (Collider2D col){
		//Debug.Log( "onTriggerStayEvent: " + col.gameObject.name );
	}

	void onTriggerExitEvent( Collider2D col ){
		//Debug.Log( "onTriggerExitEvent: " + col.gameObject.name );
	}
	#endregion

	IEnumerator reiniciaFisicas(){
		yield return new WaitForSeconds (0.02f);
		controller.SendMessage ("normalizaFisicas");
		this.rigidbody2D.isKinematic = true;
	}

	IEnumerator normalState(){
		yield return new WaitForSeconds (20.0f);
		state = 0;
		runSpeed = 8;
		for (int i=0; i<enemy.Length; i++){
			enemy[i].BroadcastMessage("enemyNormalState");
		}
	}

	//esta funcion es llamada desde landminecontroller
	void desactivaFisicas(){
		controller.SendMessage ("fisicas");
		this.rigidbody2D.isKinematic = false;
		StartCoroutine("reiniciaFisicas");
	}

	//cojemos los cursores para que se mueva el pj
	// the Update loop only gathers input. Actual movement is handled in FixedUpdate because we are using the Physics system for movement
	void Update(){

		//sonidos
		if(Input.GetKeyDown (KeyCode.S) || Input.GetKeyDown (KeyCode.JoystickButton0)){
			slideSound.Play();
		}
		if(Input.GetKeyDown (KeyCode.D)|| Input.GetKeyDown (KeyCode.JoystickButton1)){
			if(state == 0)
			punchSound.Play();
		}
		if(Input.GetKeyUp (KeyCode.S) || Input.GetKeyUp (KeyCode.JoystickButton0)){
			slideSound.Stop();
		}
		if(Input.GetKeyUp (KeyCode.D) || Input.GetKeyUp (KeyCode.JoystickButton1)){
			if(state == 0)
			punchSound.Stop();
		}
		if(Input.GetKeyDown (KeyCode.RightArrow)){
			runSound.Play();
		}
		if(Input.GetKeyDown (KeyCode.LeftArrow)){
			runSound.Play();
		}
		if(Input.GetKeyUp (KeyCode.RightArrow)){
			runSound.Stop();
		}
		if(Input.GetKeyUp (KeyCode.LeftArrow)){
			runSound.Stop();
		}

		//comandos
		if (!joy) {
			// a minor bit of trickery here. FixedUpdate sets up to false so to ensure we never miss any jump presses we leave up
			// set to true if it was true the previous frame
			up = up || Input.GetKeyDown (KeyCode.UpArrow);
			right = Input.GetKey (KeyCode.RightArrow);
			left = Input.GetKey (KeyCode.LeftArrow);
			down = Input.GetKey (KeyCode.DownArrow);
			slide = Input.GetKey (KeyCode.S);
			punch = Input.GetKey (KeyCode.D);	
		} 
		else {
			up = up || Input.GetKey (KeyCode.JoystickButton2);			
			
			if(Input.GetAxis("Horizontal") < 0){
				left = true;
			}
			else
			{
				left = false;
			}
			
			
			if(Input.GetAxis("Horizontal") > 0){
				right = true;
			}
			else
			{
				right = false;
			}

			if(Input.GetAxis("Vertical") < 0){
				down = true;
			}
			else
			{
				down = false;
			}

			slide = Input.GetKey (KeyCode.JoystickButton0);
			punch = Input.GetKey (KeyCode.JoystickButton1);
		}

	}

	void FixedUpdate(){
		// grab our current velocity to use as a base for all calculations
		velocity = controller.velocity;

		//cambio de velocidad en estado SUPER
		if (state == 1) {
			runSpeed = 15;	
		}
		//para que el pj se deje de mover cuando muere
		if (state == 3) {
			runSpeed = 0;
			jumpHeight = 0;
		}

		if (controller.isGrounded) {
			velocity.y = 0;
			SendMessage("restituirTotalHorizontalRays");
		}

		//movimientos
		if (right) {
			normalizedHorizontalSpeed = 1;

			if (transform.localScale.x < 0f)
				transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);

			if (controller.isGrounded){
				if (state == 0) {
					animator.Play (Animator.StringToHash ("Run"));
				}
				if (state == 1) {
					animator.Play (Animator.StringToHash ("SuperRun"));
				}
				if (state == 2) {
					animator.Play (Animator.StringToHash ("PistolRun"));
				}
			}
		} 
		else if (left) {
			normalizedHorizontalSpeed = -1;

			if (transform.localScale.x > 0f)
				transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);

			if (controller.isGrounded){
				if (state == 0) {
					animator.Play (Animator.StringToHash ("Run"));
				}
				if (state == 1) {
					animator.Play (Animator.StringToHash ("SuperRun"));
				}
				if (state == 2) {
					animator.Play (Animator.StringToHash ("PistolRun"));
				}
			}					
		} 
		//agacharse
		else if (down) {
			SendMessage("disminuirTotalHorizontalRays");
			circleCollider.enabled = false;

			if (state == 0) {
				animator.SetBool ("Crouch", true);
			}
			if (state == 1) {
				animator.SetBool ("SuperCrouch", true);
			}
			if (state == 2) {
				animator.SetBool ("PistolCrouch", true);
			}
		} 
		//deslizarse
		else if (slide) {
			if (controller.isGrounded) {
				if (transform.localScale.x > 0f){
					SendMessage("disminuirTotalHorizontalRays");
					circleCollider.enabled = false;

					this.rigidbody2D.isKinematic = false;
					Vector2 empuje = new Vector2 (fuerzaSlide,0);
					this.rigidbody2D.AddForce (empuje);

					if(state == 0){
						animator.Play (Animator.StringToHash ("Slide"));
					}
					if(state == 1){
						animator.Play (Animator.StringToHash ("SuperSlide"));
					}
					if(state == 2){
						animator.Play (Animator.StringToHash ("PistolSlide"));
					}
					
					StartCoroutine("reiniciaFisicas");
				}
				if (transform.localScale.x < 0f){
					SendMessage("disminuirTotalHorizontalRays");
					circleCollider.enabled = false;

					this.rigidbody2D.isKinematic = false;
					Vector2 empuje = new Vector2 (- fuerzaSlide,0);
					this.rigidbody2D.AddForce (empuje);

					if(state == 0){
						animator.Play (Animator.StringToHash ("Slide"));
					}
					if(state == 1){
						animator.Play (Animator.StringToHash ("SuperSlide"));
					}
					if(state == 2){
						animator.Play (Animator.StringToHash ("PistolSlide"));
					}

					StartCoroutine("reiniciaFisicas");
				}
			}
		} 
		//puñetazo en estado 0 y disparo en estado 2
		else if (punch) {
			if (controller.isGrounded) {
				if(state == 0){
					animator.Play (Animator.StringToHash ("Punch"));
				}
				if(state == 2){
					if (transform.localScale.x > 0f){
						animator.SetBool("Shoot",true);
						SendMessage("shootRight");//funcion de raycastweapon
					}
					if (transform.localScale.x < 0f){
						animator.SetBool("Shoot",true);
						SendMessage("shootLeft");//funcion de raycastweapon
					}
				}
			}
		}
		//estado de reposo y reseteo de variables
		else {
			normalizedHorizontalSpeed = 0;

			if (controller.isGrounded) {
				//reinicio de variables
				animator.SetBool ("Crouch", false);
				animator.SetBool("SuperCrouch",false);
				animator.SetBool("PistolCrouch",false);
				animator.SetBool("Shoot",false);

				if(state == 0){
					runSound.Stop();
					animator.Play (Animator.StringToHash ("Idle"));
				}
				if(state == 1){
					runSound.Stop();
					animator.Play (Animator.StringToHash ("SuperIdle"));
				}
				if(state == 2){
					runSound.Stop();
					animator.Play (Animator.StringToHash ("PistolIdle"));
				}
			}
		}

		//caida desde altura
		if (velocity.y < -1) {
			if(state == 0){
				animator.Play (Animator.StringToHash ("Fall"));
			}
			if(state == 1){
				animator.Play (Animator.StringToHash ("SuperFall"));
			}
			if(state == 2){
				animator.Play (Animator.StringToHash ("PistolFall"));
			}
		}

		// we can only jump whilst grounded
		if (controller.isGrounded && up) {
			velocity.y = Mathf.Sqrt (2f * jumpHeight * -gravity);
			if(state == 0){
				jumpSound.Play();
				animator.Play (Animator.StringToHash ("Jump"));
			}
			if(state == 1){
				jumpSound.Play();
				animator.Play (Animator.StringToHash ("SuperJump"));
			}
			if(state == 2){
				jumpSound.Play();
				animator.Play (Animator.StringToHash ("PistolJump"));
			}
		}		

		// apply horizontal speed smoothing it
		var smoothedMovementFactor = controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
		velocity.x = Mathf.Lerp (velocity.x, normalizedHorizontalSpeed * runSpeed, Time.fixedDeltaTime * smoothedMovementFactor);

		// apply gravity before moving
		velocity.y += gravity * Time.fixedDeltaTime;

		controller.move (velocity * Time.fixedDeltaTime);

		// reset input
		up = false;

		//obligo a que el pj no rote
		this.transform.localEulerAngles = new Vector3 (0.0f, 0.0f, 0.0f);
	}
}