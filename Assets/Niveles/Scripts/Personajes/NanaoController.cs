﻿using UnityEngine;
using System.Collections;

public class NanaoController : MonoBehaviour {

	private Animator animator;
	
	void Start(){
		animator = GetComponent<Animator> ();
	}
	
	//Esta funcion es llamada desde ActivaPanel
	void talkNanao(){
		animator.SetBool("TalkNanao",true);
	}
	
	void stopTalkNanao(){
		animator.SetBool("TalkNanao",false);
	}
}
