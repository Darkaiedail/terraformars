﻿using UnityEngine;
using System.Collections;

public class ComDavisController : MonoBehaviour {

	private Animator animator;

	void Start(){
		animator = GetComponent<Animator> ();
	}

	//Esta funcion es llamada desde ActivaPanel
	void talk(){
		animator.SetBool("Talk",true);
	}

	void stopTalk(){
		animator.SetBool("Talk",false);
	}
}
