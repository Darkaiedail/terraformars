﻿using UnityEngine;
using System.Collections;

public class BelowRayCast : MonoBehaviour {

	public FlyEnemyController fly;

	[Range( 2, 20 )]
	public int totalVerticalRays;
	public float rayLengthVer;
	private float DistanceBetweenRaysVer;

	public float distRayo;
	
	[HideInInspector]
	public BoxCollider2D boxCollider;
	
	private bool pause = true;
	
	void Awake(){
		boxCollider = GetComponent<BoxCollider2D>();

		recalculateDistanceBetweenRaysVer ();
	}

	void Update () {
		if (pause) {
			rayo();
			StartCoroutine (pausa ());	
		}
	}
	
	void recalculateDistanceBetweenRaysVer(){
		float colliderUseableWidth = boxCollider.size.x * Mathf.Abs(transform.localScale.x);
		DistanceBetweenRaysVer = colliderUseableWidth / (totalVerticalRays - 1);
	}

	IEnumerator pausa(){
		yield return new WaitForSeconds (0.7f);
		pause = true;
	}

	void rayo(){
		pause = false;

		for (int i = 0; i < totalVerticalRays; i++) {
			Vector2 origen = new Vector2 (transform.position.x - (boxCollider.size.x / 2.0f) + i * DistanceBetweenRaysVer, transform.position.y + distRayo);
			Vector2 rayDirection = -Vector2.right;
			
			RaycastHit2D hit = Physics2D.Raycast (origen, rayDirection, rayLengthVer);
			Debug.DrawRay (origen, rayDirection * rayLengthVer, Color.green);
			
			if (hit.collider != null) {
				if(hit.collider.gameObject.CompareTag("FinalBlock")){
					fly.SendMessage("gira");
				}
				else if (hit.collider.gameObject.CompareTag ("BasicBlock")) {
					fly.SendMessage("jump");
				}
			}
		}
	}
}
