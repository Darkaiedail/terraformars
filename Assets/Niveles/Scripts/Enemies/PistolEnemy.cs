﻿using UnityEngine;
using System.Collections;

public class PistolEnemy : MonoBehaviour {

	
	//variables de disparo
	private Vector2 rayDirection = Vector2.zero;
	private float cadencia = 0.8f;
	
	private bool reload = false;

	private GameObject manager;
	private Animator animator;
	private EnemyController controller;

	//stado del pj
	public int state = 0;

	//var de render
	private LineRenderer line;

	//sonidos
	public AudioSource shot;

	void Start(){
		animator = GetComponent<Animator> ();
		manager = GameObject.FindGameObjectWithTag ("Manager");

		controller = GetComponent<EnemyController>();
		line = GetComponent<LineRenderer>();
	}

	void Update(){

		Vector2 rayDirection = Vector2.right;
		Vector2 origen = new Vector2 (transform.position.x - 3.0f,transform.position.y + 0.6f);
		
		RaycastHit2D hit = Physics2D.Raycast (origen,rayDirection,6.0f);
		Debug.DrawRay (origen,rayDirection * 6.0f, Color.green);
		
		if (hit.collider != null) {
			if(hit.collider.gameObject.CompareTag("Player")){
				animator.SetBool("shot",true);
				controller.SendMessage("dispara");
				shootLeft();
			}
			else{
				animator.SetBool("shot",false);
				controller.SendMessage("walk");
			}
		}
	}

	public void shootLeft () {
		if (!reload) {
			rayDirection = -Vector2.right;
			shoot ();
			StartCoroutine("recargando");
		}
	}
	
	void shoot(){
		shot.Play();

		reload = true;

		Vector2 origen = new Vector2 (transform.position.x - 0.5f,transform.position.y + 0.7f);
		
		RaycastHit2D hit = Physics2D.Raycast (origen,rayDirection,3.0f);
		Debug.DrawRay (origen,rayDirection * 3, Color.green);

		line.enabled = true;
		
		line.SetPosition(0,new Vector3(transform.position.x - 0.14f,transform.position.y + 0.7f,0));
		line.SetPosition(1,hit.point);

		if (hit.collider != null) {
			if(state == 0){
				if(hit.collider.gameObject.CompareTag("Player") && !Input.GetKey (KeyCode.DownArrow)){
					//Debug.Log("tocado");
					manager.SendMessage("restaVidas");
					manager.SendMessage("respawnPj",0.0f);;
				}
				if(hit.collider.gameObject.CompareTag("Player") && Input.GetKey (KeyCode.DownArrow) ||
				   hit.collider.gameObject.CompareTag("Player") && Input.GetAxis("Vertical") < 0){
					
				}
			}
			if(state == 1){
				if(hit.collider.gameObject.CompareTag("Player")){
					
				}
			}
		}
	}
	
	IEnumerator recargando(){
		yield return new WaitForSeconds (cadencia);
		shot.Stop ();
		line.enabled = false;
		reload = false;
	}

	void enemySuperState(){
		state = 1;
	}

	void enemyNormalState(){
		state = 0;
	}
}
