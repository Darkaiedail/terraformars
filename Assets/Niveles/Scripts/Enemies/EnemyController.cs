﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	
	// movement config
	public float gravity = -25f;
	public float runSpeed = 8f;
	public float groundDamping = 20f; // how fast do we change direction? higher means faster
	public float inAirDamping = 5f;
	public float jumpHeight = 3f;
	
	[HideInInspector]
	private float normalizedHorizontalSpeed = 0;
	
	private CharacterController2D controller;
	private RaycastHit2D lastControllerColliderHit;
	private Vector3 velocity;
	//private Animator animator;

	public bool canJump = false;

	void Awake(){
		//inicializamos la dir en la q qeremos q vaya (izda)
		normalizedHorizontalSpeed = -1;

		//tomamos referencias de los componentes a modificar desde este script
		controller = GetComponent<CharacterController2D> ();
		//animator = GetComponent<Animator> ();

		//conexiones con char cont 2d para detectar los raycast y colisiones de tipo trigger
		controller.onControllerCollidedEvent += onControllerCollider;
		controller.onTriggerEnterEvent += onTriggerEnterEvent;
		controller.onTriggerExitEvent += onTriggerExitEvent;
	}

	#region Event Listeners
	void onControllerCollider(RaycastHit2D hit){
		if (controller.collisionState.left || controller.collisionState.right) {

			if (hit.collider.gameObject.CompareTag ("FinalBlock")) {
				normalizedHorizontalSpeed *= -1;//invierte el signo y por tanto el sentido
			}
			else if (hit.collider.gameObject.CompareTag ("BasicBlock")) {
				canJump = true;//salta al encontrarse un basic block
			}
		}
	}
	
	void onTriggerEnterEvent(Collider2D col){
	}
	
	void onTriggerExitEvent(Collider2D col){
	}
	#endregion

	void dispara(){
		runSpeed = 0;
		jumpHeight = 0;
	}

	void walk(){
		runSpeed = 3;
		jumpHeight = 0.5f;
	}
	//movimiento de la tortuga
	void FixedUpdate(){
				
		// grab our current velocity to use as a base for all calculations
		velocity = controller.velocity;

		if (controller.isGrounded) {
			velocity.y = 0;
		}

		//hace salto
		if (controller.isGrounded && canJump) {
			velocity.y = Mathf.Sqrt (2f* jumpHeight * -gravity);		
		}
		//desactiva el loop del salto para que solo haga uno si se pone esta variable a true
		canJump = false;

		//rotamos el pj en una dir o en otra
		if (normalizedHorizontalSpeed == 1){//dcha
			//cuando en un if solo hay una linea no hace falta poner las llaves
			if (transform.localScale.x > 0f)
				transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
		} 
		else if (normalizedHorizontalSpeed == -1) {//izda
			if (transform.localScale.x < 0f)
				transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
		}

		// apply horizontal speed smoothing it
		var smoothedMovementFactor = controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
		velocity.x = Mathf.Lerp (velocity.x, normalizedHorizontalSpeed * runSpeed, Time.fixedDeltaTime * smoothedMovementFactor);
		
		// apply gravity before moving
		velocity.y += gravity * Time.fixedDeltaTime;
		
		controller.move (velocity * Time.fixedDeltaTime);
	}
}
