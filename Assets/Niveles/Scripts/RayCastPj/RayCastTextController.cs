﻿using UnityEngine;
using System.Collections;

public class RayCastTextController : MonoBehaviour {

	private GameObject canvas;

	void Start(){
		canvas = GameObject.FindGameObjectWithTag("Canvas");
	}

	void Update(){
		Vector2 rayDirection = Vector2.right;
		Vector2 origen = new Vector2 (transform.position.x - 1.0f,transform.position.y + 0.2f);
		
		RaycastHit2D hit = Physics2D.Raycast (origen,rayDirection,2.0f);
		Debug.DrawRay (origen,rayDirection * 2.0f, Color.green);
		
		if (hit.collider != null) {
			//para hacer que aparezcan los textos
			if(hit.collider.gameObject.CompareTag("TextDetector")){
				hit.collider.SendMessage("activaTexto");
			}
			//para hace un fade to black
			if(hit.collider.gameObject.CompareTag("Fader")){
				canvas.SendMessage("FadeIn");
			}
		}
	}
}
