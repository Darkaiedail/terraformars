﻿using UnityEngine;
using System.Collections;

public class WindyZoneRayCast : MonoBehaviour {

	[Range( 2, 20 )]
	public int totalHorizontalRays;
	private float rayLengthHz;
	private float DistanceBetweenRaysHz;
	
	[HideInInspector]
	public BoxCollider boxCollider;

	private float maxWindSpeed = 3;

	void Awake(){
		boxCollider = GetComponent<BoxCollider>();

		recalculateDistanceBetweenRaysHz ();
	}
	
	void Update () {
		
		for (int i = 0; i < totalHorizontalRays; i++) {
			Vector2 origen = new Vector2 (transform.position.x - 0.5f , transform.position.y - (boxCollider.size.y / 2.0f) + i * DistanceBetweenRaysHz);
			Vector2 rayDirection = -Vector2.right;

			rayLengthHz = Random.Range(0,30); 

			RaycastHit2D hit = Physics2D.Raycast (origen, rayDirection, rayLengthHz);
			Debug.DrawRay (origen, rayDirection * rayLengthHz, Color.yellow);
			
			if (hit.collider != null) {
				if (hit.collider.gameObject.CompareTag ("Player")) {
					//Debug.Log("tocado");
					hit.collider.SendMessage("desactivaFisicas");

					float windSpeed = Random.Range(0,maxWindSpeed);
					Vector2 newPosition = hit.collider.rigidbody2D.position + windSpeed * rayDirection * Time.deltaTime;
					hit.collider.rigidbody2D.MovePosition(newPosition);
				}
			}
		}
	}

	void recalculateDistanceBetweenRaysHz(){
		float colliderUseableHeigth = boxCollider.size.y * Mathf.Abs(transform.localScale.y);
		DistanceBetweenRaysHz = colliderUseableHeigth / (totalHorizontalRays - 1);
	}
}
