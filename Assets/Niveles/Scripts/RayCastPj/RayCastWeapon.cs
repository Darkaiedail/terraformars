﻿using UnityEngine;
using System.Collections;

public class RayCastWeapon : MonoBehaviour {

	//variables de disparo
	private Vector2 rayDirection = Vector2.zero;
	[Range(0,1)]
	public float cadencia = 0.5f;

	private bool reload = false;

	//variables de efectos
	public ParticleSystem particulas;

	//sonidos
	public AudioSource shot;

	//esta funcion es llamada desde phisicsplayestester
	public void shootRight () {
		if (!reload) {
			shot.Play();

			rayDirection = Vector2.right;
			shoot ();
			StartCoroutine("recargando");
		}
	}

	//esta funcion es llamada desde phisicsplayestester
	public void shootLeft () {
		if (!reload) {
			shot.Play();

			rayDirection = -Vector2.right;
			shoot ();
			StartCoroutine("recargando");
		}
	}

	void shoot(){
		reload = true;

		particulas.Emit(1);

		Vector2 origen = new Vector2 (transform.position.x + 0.75f,transform.position.y + 1.05f);
		
		RaycastHit2D hit = Physics2D.Raycast (origen,rayDirection,7.0f);
		Debug.DrawRay (origen,rayDirection * 7, Color.green);
		
		if (hit.collider != null) {
			if(hit.collider.gameObject.CompareTag("Enemy")){
				Destroy(hit.collider.gameObject);
			}
		}
	}
	
	IEnumerator recargando(){
		yield return new WaitForSeconds (cadencia);
		reload = false;
	}
}
