﻿using UnityEngine;
using System.Collections;

public class AboveRayCast : MonoBehaviour {

	[Range( 2, 20 )]
	public int totalVerticalRays;
	public float rayLengthVer;
	private float DistanceBetweenRaysVer;

	[HideInInspector]
	public BoxCollider2D boxCollider;

	private GameObject manager;

	void Awake(){
		boxCollider = GetComponent<BoxCollider2D>();
		manager = GameObject.FindGameObjectWithTag("Manager");

		recalculateDistanceBetweenRaysVer ();
	}

	void Update () {

		for (int i = 0; i < totalVerticalRays; i++) {
			Vector2 origen = new Vector2 (transform.position.x - (boxCollider.size.x / 2.0f) + i * DistanceBetweenRaysVer, transform.position.y + 1.2f);
			Vector2 rayDirection = Vector2.up;

			RaycastHit2D hit = Physics2D.Raycast (origen, rayDirection, rayLengthVer);
			Debug.DrawRay (origen, rayDirection * rayLengthVer, Color.green);

			if (hit.collider != null) {
				if (hit.collider.gameObject.CompareTag ("FallingBlocks")) {
					manager.SendMessage("restaVidas");
					manager.SendMessage("respawnPj",0.0f);
				}
			}
		}
	}

	void recalculateDistanceBetweenRaysVer(){
		float colliderUseableWidth = boxCollider.size.x * Mathf.Abs(transform.localScale.x);
		DistanceBetweenRaysVer = colliderUseableWidth / (totalVerticalRays - 1);
	}
}
