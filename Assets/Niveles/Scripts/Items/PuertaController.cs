﻿using UnityEngine;
using System.Collections;

public class PuertaController : MonoBehaviour {

	private Animator animator;

	public AudioSource open;

	void Start () {
		animator = GetComponent<Animator>();
	}

	//esta funcion es llamada desde manager
	void abrir(){
		open.Play ();

		gameObject.rigidbody2D.isKinematic = true;
		gameObject.collider2D.enabled = false;

		animator.Play(Animator.StringToHash ("Entrada"));
	}

	//esta funcion es llamada desde manager
	void cerrar(){
		open.Play ();

		animator.Play(Animator.StringToHash ("Salida"));
	}
}
