﻿using UnityEngine;
using System.Collections;

public class BalaController : MonoBehaviour {

	public float speed = 10.0f;

	private GameObject manager;
	
	void Start () {
		manager = GameObject.FindGameObjectWithTag ("Manager");
	}
		
	void Update () {
		//para que el proyectil se mueva en el eje z
		this.transform.Translate (Vector2.right * Time.deltaTime * speed);

		Destroy (gameObject,0.5f);

		Vector2 rayDirection = Vector2.right;
		Vector2 origen = new Vector2 (transform.position.x - 0.5f,transform.position.y);
		
		RaycastHit2D hit = Physics2D.Raycast (origen,rayDirection,1.0f);
		Debug.DrawRay (origen,rayDirection * 1.0f, Color.green);

		Vector2 rayDirectionVer = Vector2.up;
		Vector2 origenVer = new Vector2 (transform.position.x,transform.position.y - 0.5f);
		
		RaycastHit2D hitVer = Physics2D.Raycast (origenVer,rayDirectionVer,1.0f);
		Debug.DrawRay (origenVer,rayDirectionVer * 1.0f, Color.red);
		
		if (hit.collider != null) {

			if(hit.collider.gameObject.CompareTag("Player")){
				Destroy(this.gameObject);
				manager.SendMessage("restaVidas");
				manager.SendMessage("respawnPj",0.0f);
			}
		}
		if (hitVer.collider != null) {

			if(hitVer.collider.gameObject.CompareTag("Player")){
				Destroy(this.gameObject);
				manager.SendMessage("restaVidas");
				manager.SendMessage("respawnPj",0.0f);
			}
		}
	}
}
