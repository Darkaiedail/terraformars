﻿using UnityEngine;
using System.Collections;

public class BoxController : MonoBehaviour {

	private Animator animator;
	public GameObject pistol;

	//para controlar que solo instancia un item
	private bool finishTakeItem = false;

	//sonidos
	public AudioSource pistolAppears;

	void Start () {
		animator = GetComponent<Animator>();
	}
	
	void openBox () {
		animator.Play(Animator.StringToHash ("open"));
	}
		
	void instanciaPistola(){
		if (!finishTakeItem) {
			pistolAppears.Play();

			finishTakeItem = true;
			GameObject gun = Instantiate (pistol, this.transform.position, Quaternion.identity) as GameObject;
			iTween.MoveAdd (gun.gameObject, iTween.Hash ("y",2.5f,
			                                             "x",0.3f,
			                                             "time", 0.3f,
			                                             "easetype", "linear"));
		}
	}
}
