﻿using UnityEngine;
using System.Collections;

public class FallingBlockDetector : MonoBehaviour {
	public GameObject[] fallingBlock;

	void fallBlocks(){
		for (int i=0; i<fallingBlock.Length; i++){
			fallingBlock[i].SendMessage("fall");
		}
	}
}
