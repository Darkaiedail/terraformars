﻿using UnityEngine;
using System.Collections;

public class AddSlideController : MonoBehaviour {

	void addSlideForce(){
		StartCoroutine ("colliderOn");
	}

	IEnumerator colliderOn(){
		yield return new WaitForSeconds (1.0f);
		this.gameObject.collider2D.enabled = true;
	}
}
