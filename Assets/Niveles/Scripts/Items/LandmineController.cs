﻿using UnityEngine;
using System.Collections;

public class LandmineController : MonoBehaviour {

	public GameObject partesMina;
	public GameObject particles;

	private Animator animator;
	private GameObject manager;

	//variables para restar vida en funcion de distancia entre mina y pj
	private GameObject player;
	private float absDistanceX;
	private float absDistanceY;

	//variables para aplicar una fuerza de explosion
	private float distanceX;

	//sonidos
	public AudioSource explosion;

	void Start(){
		animator = GetComponent<Animator>();

		manager = GameObject.FindGameObjectWithTag("Manager");
	}

	void Update(){
		player = GameObject.FindGameObjectWithTag("Player");
			
		if(!player){
			player = GameObject.FindGameObjectWithTag("NaveInicio");
		}

		float landMinePositionX = this.transform.position.x;
		float playerPositionX = player.transform.position.x;

		absDistanceX = Mathf.Abs(playerPositionX - landMinePositionX);

		distanceX = playerPositionX - landMinePositionX;

		float landMinePositionY = this.transform.position.y;
		float playerPositionY = player.transform.position.y;
		
		absDistanceY = Mathf.Abs(playerPositionY - landMinePositionY);

		if (absDistanceX <= 3.0f && absDistanceY <= 3.0f){
			cuentaAtras();
			StartCoroutine(rompe());
		}
	}

	void cuentaAtras(){
		animator.Play (Animator.StringToHash ("Explosion"));
	}

	//esta funcion es llamada desde physicsPlayerTester
	IEnumerator rompe(){
		yield return new WaitForSeconds (1.0f);
		explosion.Play ();

		Instantiate (particles, this.transform.position, this.transform.rotation);
		Instantiate (partesMina, this.transform.position, this.transform.rotation);

		player.SendMessage ("desactivaFisicas");
		aplicaFuerza ();

		Destroy (this.gameObject);

		if(absDistanceX <= 5.0f && absDistanceY <= 5.0f){
			manager.SendMessage("restaVidas");
		}
	}

	void aplicaFuerza(){
			if(distanceX <= 5.0f){
				player.rigidbody2D.AddForce (new Vector2(-2000,500));
			}
			if(distanceX >= -5.0f){
				player.rigidbody2D.AddForce (new Vector2(2000,500));
			}
	}
}