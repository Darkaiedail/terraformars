﻿using UnityEngine;
using System.Collections;

public class PPioNivel : MonoBehaviour {

	public float delay;

	// Use this for initialization
	void Start () {
		StartCoroutine (begin());
	}
	
	IEnumerator begin(){
		yield return new WaitForSeconds (delay);
		this.collider2D.enabled = false;
	}
}
