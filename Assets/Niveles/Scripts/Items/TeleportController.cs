﻿using UnityEngine;
using System.Collections;

public class TeleportController : MonoBehaviour {

	private Animator animator;

	//sonidos
	public AudioSource teleport;

	void Start () {
		animator = GetComponent<Animator>();
		StartCoroutine (activaAnimacion());
	}

	IEnumerator activaAnimacion(){
		yield return new WaitForSeconds (2.0f);
		teleport.Play ();
		animator.Play(Animator.StringToHash ("Teleport"));
		Destroy (this.gameObject, 0.8f);
	}
}
