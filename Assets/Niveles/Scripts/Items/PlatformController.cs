﻿using UnityEngine;
using System.Collections;
using System;

public class PlatformController : MonoBehaviour {

	//variables tipo de movimiento
	public enum Movimiento{Linear,Circular};
	public Movimiento mov = Movimiento.Linear;

	//variables de mov hz
	public enum DireccionH {Izda,Dcha};
	public DireccionH sentidoH = DireccionH.Dcha;
	public float velocidadH = 0.3f;

	//variables de mov vert
	public enum DireccionV {Arriba,Abajo};
	public DireccionV sentidoV = DireccionV.Arriba;
	public float velocidadV = 0.0f;	

	
	//distancia que recorre la plat en modo pingpong(para que no lo haga = -1)
	public float maxRecorridoPingPong = 5.0f;

	//variables de mov circular
	public float radius = 10f;
	public float velocidadCirc = 1f;
	public bool offsetIsCenter = true;
	public Vector3 offset;

	private Transform platformTransform;
	private float walkedDistanceH = 0.0f;
	private float walkedDistanceV = 0.0f;
	private float referencePingPongPositionH;
	private float referencePingPongPositionV;

	void Start () {
		//pillamos el transform de la plataforma
		platformTransform = transform;

		//posiciones de referencia para el ping pong hz y vert
		referencePingPongPositionH = Math.Abs (platformTransform.position.x);
		referencePingPongPositionV = Math.Abs (platformTransform.position.y);

		if(offsetIsCenter){
			offset = transform.position;
		}
	}
	
	void Update () {
		//mov linear
		if (mov == Movimiento.Linear) {
			//distancia recorrida en el ping pong
			walkedDistanceH = Math.Abs (Math.Abs (platformTransform.position.x) - referencePingPongPositionH);
			walkedDistanceV = Math.Abs (Math.Abs (platformTransform.position.y) - referencePingPongPositionV);
			
			//pingpong hz
			if (maxRecorridoPingPong != -1 && walkedDistanceH >= maxRecorridoPingPong) {
				if(sentidoH == DireccionH.Izda){
					sentidoH = DireccionH.Dcha;
				}
				else{
					sentidoH = DireccionH.Izda;
				}
				referencePingPongPositionH = Math.Abs(platformTransform.position.x);
			}
			//pingpong vert
			if (maxRecorridoPingPong != -1 && walkedDistanceV >= maxRecorridoPingPong) {
				if(sentidoV == DireccionV.Arriba){
					sentidoV = DireccionV.Abajo;
				}
				else{
					sentidoV = DireccionV.Arriba;
				}
				referencePingPongPositionV = Math.Abs(platformTransform.position.y);
			}
			//mov hz
			if (sentidoH == DireccionH.Izda) {
				velocidadH = -Math.Abs (velocidadH);	
			} else {
				velocidadH = Math.Abs (velocidadH);
			}
			//mov vert
			if (sentidoV == DireccionV.Abajo) {
				velocidadV = -Math.Abs (velocidadV);	
			} else {
				velocidadV = Math.Abs (velocidadV);
			}
			//movimiento de la plataforma
			platformTransform.Translate (new Vector3 (velocidadH, velocidadV, 0) * Time.deltaTime);
		}

		//mov circular
		if(mov == Movimiento.Circular){
			transform.position = new Vector3((radius * Mathf.Cos(Time.time*velocidadCirc))+offset.x,
											 (radius * Mathf.Sin(Time.time*velocidadCirc))+offset.y,
											  offset.z);
		}
	}
}
