﻿using UnityEngine;
using System.Collections;

public class CameraUpDown : MonoBehaviour {

	public float minDcha;
	public float minIzda;
	private GameObject camera;

	void Start(){
		camera = GameObject.FindGameObjectWithTag("MainCamera");
	}

	//esta funcion es llamada desde physicsplayertester
	void minCamaraDcha(){
		camera.SendMessage ("desbloquearCamara",minDcha);
		this.gameObject.collider2D.enabled = false;
		StartCoroutine (colliderOn ());
	}

	//esta funcion es llamada desde physicsplayertester
	void minCamaraIzda(){
		camera.SendMessage ("desbloquearCamara",minIzda);
		this.gameObject.collider2D.enabled = false;
		StartCoroutine (colliderOn ());
	}

	IEnumerator colliderOn(){
		yield return new WaitForSeconds (1.0f);
		this.gameObject.collider2D.enabled = true;
	}
}
