﻿using UnityEngine;
using System.Collections;

public class FallingBlockController : MonoBehaviour {

	public float delay;
	public float timeShake;

	//sonidos
	public AudioSource fallSound;
	//public AudioSource shakeSound;

	//esta funcion es llamada desde physics player tester
	void fall(){
		StartCoroutine (falling ());
		//shakeSound.Play();
		iTween.ShakePosition (this.gameObject, iTween.Hash ("x", 0.1f,
		                                                    "time", timeShake));
	}

	IEnumerator falling(){
		yield return new WaitForSeconds (delay);
		fallSound.Play ();
		gameObject.rigidbody2D.isKinematic = false;
	}
}
