﻿using UnityEngine;
using System.Collections;

public class PartLandmine : MonoBehaviour {

	public Vector2 direction = new Vector2(0,0);
	//public Vector2 rotation = new Vector2(0,0);

	void Start () {

		Rigidbody2D rb = GetComponent<Rigidbody2D> ();
		rb.AddForce (direction);

		Destroy (this.gameObject, 2.0f);
	}
}
