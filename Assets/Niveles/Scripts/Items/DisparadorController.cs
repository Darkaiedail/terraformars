﻿using UnityEngine;
using System.Collections;

public class DisparadorController : MonoBehaviour {

	public GameObject bala;
	private bool canShoot = true;

	public Transform disparador;

	void Update () {
		//para que el enemigo dispare
		if (canShoot == true){
			shoot();
			StartCoroutine("reload");
		}
	}

	IEnumerator reload(){
		yield return new WaitForSeconds (1.0f);
		canShoot = true;
	}

	void shoot(){
		canShoot = false;
		Instantiate (bala, disparador.position, Quaternion.identity);
	}
}
