﻿using UnityEngine;
using System.Collections;

public class LaserController : MonoBehaviour {
	
	//varables de control de laser
	[Range(0,10)]
	public float[] cadencia;
	public Vector2[] rayDirection;
	[Range(-0.5f,0.5f)]
	public float[] desplazX;
	[Range(-0.5f,0.5f)]
	public float[] desplazY;
	[Range(0,10)]
	public float[] tiempoDisparo;
	
	private bool reload = false;
	
	private GameObject manager;

	//var de render
	private LineRenderer line;

	//sonidos
	public AudioSource laser;

	void Start(){
		manager = GameObject.FindGameObjectWithTag ("Manager");

		line = GetComponent<LineRenderer>();
	}
	
	void Update(){
		if (!reload) {
			StartCoroutine("recargando");
			StartCoroutine ("shoot");		
		}
	}

	IEnumerator shoot(){
		laser.Play ();

		for (int j = 0; j < cadencia.Length; j++) {
			yield return new WaitForSeconds (cadencia[j]);

			reload = false;
			
			Vector2 origen = new Vector2 (transform.position.x + desplazX[j],transform.position.y + desplazY[j]);
			
			RaycastHit2D hit = Physics2D.Raycast (origen,rayDirection[j],7.0f);
			Debug.DrawRay (origen,rayDirection[j] * 7, Color.green);
			
			line.enabled = true;
			
			line.SetPosition(0,new Vector3(transform.position.x + desplazX[j],transform.position.y + desplazY[j],0));
			line.SetPosition(1,hit.point);
			//line.SetPosition(1,rayDirection[j]);

			if (hit.collider != null) {
				if(hit.collider.gameObject.CompareTag("Player")){
					//Debug.Log("tocado");
					manager.SendMessage("restaVidas");
					manager.SendMessage("respawnPj",0.0f);
				}
			}
		}
	}

	IEnumerator recargando(){


		for (int j = 0; j < cadencia.Length; j++) {
			yield return new WaitForSeconds (tiempoDisparo [j]);
			laser.Stop ();

			line.enabled = false;
			reload = true;
		}
	}
}
