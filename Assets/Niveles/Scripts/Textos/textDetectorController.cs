﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class textDetectorController : MonoBehaviour {

	public int numText;
	public GameObject panelText;
	public float tiempoTexto;

	private GameObject manager;
	//private GameObject camara;

	void Start(){		
		manager = GameObject.FindGameObjectWithTag("Manager");
		//camara = GameObject.FindGameObjectWithTag ("MainCamera");
	}

	void activaTexto(){
			this.gameObject.collider2D.enabled = false;
			panelText.SetActive (true);
			manager.SendMessage ("readXml",numText);
			StartCoroutine (desactivaTexto());	
	}

	IEnumerator desactivaTexto(){
		yield return new WaitForSeconds (tiempoTexto);
		panelText.SetActive (false);
	}
}
