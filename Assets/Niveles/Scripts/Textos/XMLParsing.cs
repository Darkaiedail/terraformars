﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using UnityEngine.UI;

public class XMLParsing : MonoBehaviour{

	public Text fileDataTextbox;
	public Text fileDataTextboxDialogo;
	private XmlDocument xmlDoc;
	public TextAsset instrucciones;

	private List<string> listaInstrucciones = new List<string>();

	//sonidos
	public AudioSource letras;

	// Structure for mainitaing the player information
//	struct Player{
//		public int Id;
//		public string name;
//		public int score;
//		public string fecha;
//	};

	void Start(){
		xmlDoc = new XmlDocument();
		xmlDoc.LoadXml (instrucciones.text);
 		//readXml();
	}

	// Following method reads the xml file and display its content 
	public void readXml(int numText){

		foreach(XmlElement node in xmlDoc.SelectNodes("instrucciones/instruccion")){
			fileDataTextbox.text = "";
			fileDataTextboxDialogo.text = "";

			string textoInstruccionTemp = node.SelectSingleNode("texto").InnerText;
			listaInstrucciones.Add(textoInstruccionTemp);
		}

		StartCoroutine ("generaFraseAnimada",numText);

		//Acaba el parse Instrucciones

		//fileDataTextbox.text = listaInstrucciones[0];// /t->espacio /n->intro
	}

	IEnumerator generaFraseAnimada(int numInstruccion){
		char [] caracteres = listaInstrucciones [numInstruccion].ToCharArray();
		//Debug.Log (caracteres [0]);
		foreach (char character in caracteres) {
			letras.Play();

			fileDataTextbox.text +=  character.ToString();
			fileDataTextboxDialogo.text += character.ToString();
			yield return new WaitForSeconds(0.05f);
		}
		//fileDataTextbox.text = listaInstrucciones[0];
	}

//	private void displayPlayerData(Player tempPlayer){
//		fileDataTextbox.text += tempPlayer.fecha + "\t\t" + tempPlayer.name + "\t\t\t" + tempPlayer.score + "\n";// /t->espacio /n->intro
//	}
}