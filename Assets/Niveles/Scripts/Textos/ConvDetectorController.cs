﻿using UnityEngine;
using System.Collections;

public class ConvDetectorController : MonoBehaviour {

	//variables para sacar los dialogos en pantalla
	public GameObject panelText;
	public int[] numText;
	public float[] tiempoTexto;
	
	private GameObject manager;
	//private GameObject camara;
	
	void Start(){		
		manager = GameObject.FindGameObjectWithTag("Manager");
		//camara = GameObject.FindGameObjectWithTag ("MainCamera");
	}
	
	void activaTexto(){
		this.gameObject.collider2D.enabled = false;
		panelText.SetActive (true);
		manager.SendMessage ("readXml",numText[0]);
		StartCoroutine (desactivaTexto());
		StartCoroutine (activaSiguienteTexto ());
	}
	
	IEnumerator desactivaTexto(){
		yield return new WaitForSeconds (tiempoTexto[0]);
		panelText.SetActive (false);
	}

	IEnumerator activaSiguienteTexto(){
		yield return new WaitForSeconds (tiempoTexto[0]);
		this.gameObject.collider2D.enabled = false;
		panelText.SetActive (true);
		manager.SendMessage ("readXml",numText[1]);
		StartCoroutine (desactivaSiguienteTexto());
	}
	
	IEnumerator desactivaSiguienteTexto(){
		yield return new WaitForSeconds (tiempoTexto[1]);
		panelText.SetActive (false);
	}
}
