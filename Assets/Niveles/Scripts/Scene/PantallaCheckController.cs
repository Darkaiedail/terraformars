﻿using UnityEngine;
using System.Collections;

public class PantallaCheckController : MonoBehaviour {

	private Animator animator;

	//sonidos
	public AudioSource checkpoint;

	void Start(){
		animator = GetComponent<Animator> ();
	}

	void checkPoint(){
		checkpoint.Play ();

		animator.SetBool ("CheckPoint", true);
	}
}
