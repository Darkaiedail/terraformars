﻿using UnityEngine;
using System.Collections;

public class ConstructScene : MonoBehaviour {

	//piezas para el escenario
	public GameObject basicBlock;
	public GameObject mystBlock;
	public GameObject tubeTop;
	public GameObject tube;
	public GameObject turtle;

	public TextAsset mapAsset;

	public Vector3 posInit;

	void Start () {
	
		string[] mapCharacter = mapAsset.text.Split("\n"[0]);//array que encuentra cada linea hasta que encuentre un salto de linea (/n)

		Vector3 position = new Vector3 ();

		float j_offSet = mapCharacter.Length / 2.0f;

		GameObject newInstance;

		for (int j = 0; j < mapCharacter.Length; j++) {//Aqui leemos filas
			position.y = (mapCharacter.Length - j - j_offSet - 1)*1;

			float i_offSet = mapCharacter[j].Length / 50.0f;

			for(int i = 0; i < mapCharacter[j].Length; i++){//Aqui leemos columnas
				position.x = (i - i_offSet)*1;

				if(mapCharacter[j][i].Equals('X')){
					newInstance = Instantiate(basicBlock, position, Quaternion.identity) as GameObject;
					newInstance.transform.parent = this.transform;//para colocarlo como hijo de escenario
				}
				if(mapCharacter[j][i].Equals('M')){
					newInstance = Instantiate(mystBlock, position, Quaternion.identity) as GameObject;
					newInstance.transform.parent = this.transform;//para colocarlo como hijo de escenario
				}
				if(mapCharacter[j][i].Equals('T')){
					newInstance = Instantiate(tubeTop, position, Quaternion.identity) as GameObject;
					newInstance.transform.parent = this.transform;//para colocarlo como hijo de escenario
				}
				if(mapCharacter[j][i].Equals('U')){
					newInstance = Instantiate(tube, position, Quaternion.identity) as GameObject;
					newInstance.transform.parent = this.transform;//para colocarlo como hijo de escenario
				}
			}
		}	
	}
}
