﻿using UnityEngine;
using System.Collections;

public class CheckPointController : MonoBehaviour {

	public GameObject Pantalla;

	private Animator animator;

	void Start(){
		animator = GetComponent<Animator> ();
	}

	void checkPoint(){
		iTween.ScaleAdd (this.gameObject, iTween.Hash ("y", - 0.1f,
		                                              "time", 0.1f,
		                                              "easetype", "linear"));
		iTween.MoveAdd (this.gameObject, iTween.Hash ("y", - 0.1f,
		                                               "time", 0.1f,
		                                               "easetype", "linear"));
		animator.SetBool("PressButton",true);

		Pantalla.SendMessage ("checkPoint");
	}
}
