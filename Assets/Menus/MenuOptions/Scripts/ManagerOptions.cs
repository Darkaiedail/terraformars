﻿using UnityEngine;
using System.Collections;

public class ManagerOptions : MonoBehaviour {

	public GameObject[] panels;
	public GameObject panelOptions;

	//sonidos
	public AudioSource select;
	public AudioSource button;
	
	public void returnMenu(){
		if (Input.GetKey (KeyCode.Return) || Input.GetKey(KeyCode.JoystickButton0)){
			button.Play();
			Application.LoadLevel(0);
		}	
	}
	
	public void sound(){
		if (Input.GetKey (KeyCode.Return) || Input.GetKey (KeyCode.JoystickButton0)) {
			button.Play();
			panels [0].SetActive (true);
			panelOptions.SetActive(false);
		}
	}
	
	public void joy(){
		if (Input.GetKey (KeyCode.Return) || Input.GetKey(KeyCode.JoystickButton0)){
			button.Play();
			Application.LoadLevel(2);
		}
	}
	
	void Update(){
		if (Input.GetKey (KeyCode.Escape)) {
			for (int i=0; i<panels.Length; i++){
				button.Play();
				panels[i].SetActive(false);
				panelOptions.SetActive(true);
			}	
		}
		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			select.Play();		
		}
		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			select.Play();		
		}
	}
}
