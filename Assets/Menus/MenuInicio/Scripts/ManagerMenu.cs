﻿using UnityEngine;
using System.Collections;

public class ManagerMenu : MonoBehaviour {

	public GameObject menuInicio;

	public GameObject[] panel;
	public int i = 0;

	//sonidos
	public AudioSource select;
	public AudioSource button;

	public void startGame(){
		if (Input.GetKey (KeyCode.Return) || Input.GetKey(KeyCode.JoystickButton0)){
			button.Play();
			Application.LoadLevel(4);
		}	
	}

	public void controls(){
		if (Input.GetKey (KeyCode.Return) || Input.GetKey(KeyCode.JoystickButton0)){
			button.Play();
			Application.LoadLevel(3);
		}	
	}

	public void Options(){
		if (Input.GetKey (KeyCode.Return) || Input.GetKey(KeyCode.JoystickButton0)){
			button.Play();
			Application.LoadLevel(1);
		}	
	}


	void Update(){
		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			i++;
			if (i == 0) {
				panel [0].SetActive (true);
				panel [1].SetActive (false);
				panel [2].SetActive (false);
				panel [3].SetActive (false);
				panel [4].SetActive (false);
			}
			if (i == 1) {
				panel [1].SetActive (true);
				panel [0].SetActive (false);
				panel [2].SetActive (false);
				panel [3].SetActive (false);
				panel [4].SetActive (false);
			}
			if (i == 2) {
				panel [2].SetActive (true);
				panel [0].SetActive (false);
				panel [1].SetActive (false);
				panel [3].SetActive (false);
				panel [4].SetActive (false);
			}
			if (i == 3) {
				panel [3].SetActive (true);
				panel [0].SetActive (false);
				panel [1].SetActive (false);
				panel [2].SetActive (false);
				panel [4].SetActive (false);
			}
			if (i == 4) {
				panel [4].SetActive (true);
				panel [0].SetActive (false);
				panel [1].SetActive (false);
				panel [2].SetActive (false);
				panel [3].SetActive (false);
			}

			if (i > 4) {
				i = 0;
			}

			select.Play();
		}
		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			i--;
			if (i == 0) {
				panel [0].SetActive (true);
				panel [1].SetActive (false);
				panel [2].SetActive (false);
				panel [3].SetActive (false);
				panel [4].SetActive (false);
			}
			if (i == 1) {
				panel [1].SetActive (true);
				panel [0].SetActive (false);
				panel [2].SetActive (false);
				panel [3].SetActive (false);
				panel [4].SetActive (false);
			}
			if (i == 2) {
				panel [2].SetActive (true);
				panel [0].SetActive (false);
				panel [1].SetActive (false);
				panel [3].SetActive (false);
				panel [4].SetActive (false);
			}
			if (i == 3) {
				panel [3].SetActive (true);
				panel [0].SetActive (false);
				panel [1].SetActive (false);
				panel [2].SetActive (false);
				panel [4].SetActive (false);
			}
			if (i == 4) {
				panel [4].SetActive (true);
				panel [0].SetActive (false);
				panel [1].SetActive (false);
				panel [2].SetActive (false);
				panel [3].SetActive (false);
			}

			if (i < 0) {
				i = 0;
			}

			select.Play();
		}
	}
}
