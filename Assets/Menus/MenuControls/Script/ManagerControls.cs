﻿using UnityEngine;
using System.Collections;

public class ManagerControls : MonoBehaviour {

	public GameObject[] panels;

	//sonidos
	public AudioSource select;
	public AudioSource button;

	public void returnMenu(){
		if (Input.GetKey (KeyCode.Return) || Input.GetKey(KeyCode.JoystickButton0)){
			button.Play();
			Application.LoadLevel(0);
		}	
	}

	public void movements(){
		if (Input.GetKey (KeyCode.Return) || Input.GetKey (KeyCode.JoystickButton0)) {
			button.Play();
			panels [0].SetActive (true);
		}
	}

	public void attacks(){
		if (Input.GetKey (KeyCode.Return) || Input.GetKey(KeyCode.JoystickButton0)){
			button.Play();
			panels [1].SetActive (true);
		}
	}

	public void bonus(){
		if (Input.GetKey (KeyCode.Return) || Input.GetKey (KeyCode.JoystickButton0)) {
			button.Play();
			panels [2].SetActive (true);
		}
	}

	void Update(){
		if (Input.GetKey (KeyCode.Escape)) {
			for (int i=0; i<panels.Length; i++){
				button.Play();
				panels[i].SetActive(false);
			}	
		}
		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			select.Play();		
		}
		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			select.Play();		
		}
	}
}
