﻿using UnityEngine;
using System.Collections;

public class ManagerJoy : MonoBehaviour {
	
	public GameObject[] panelsTic;

	//sonidos
	public AudioSource select;
	public AudioSource button;

	public void joyOff(){
		button.Play();

		panelsTic [0].SetActive (true);
		panelsTic [1].SetActive (false);
		
		PhysicsPlayerTester.joy = false;
	}
	
	public void joyOn(){
		button.Play();

		panelsTic [1].SetActive (true);
		panelsTic [0].SetActive (false);
		
		PhysicsPlayerTester.joy = true;
	}
	
	void Update(){
		if (Input.GetKey (KeyCode.Escape)) {
			button.Play();
			Application.LoadLevel(1);	
		}
		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			select.Play();		
		}
		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			select.Play();		
		}
	}
}
