﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using UnityEngine.UI;

public class XML : MonoBehaviour{
		
	public Text fileDataTextbox;
	public Text fileDataTextboxDown;
	private XmlDocument xmlDoc;
	public TextAsset conversaciones;

	//sonidos
	public AudioSource letras;
	
	private List<string> listaConversaciones = new List<string>();

	// Structure for mainitaing the player information
	//	struct Player{
	//		public int Id;
	//		public string name;
	//		public int score;
	//		public string fecha;
	//	};
	
	void Start(){
		xmlDoc = new XmlDocument();
		xmlDoc.LoadXml (conversaciones.text);

		//readXml(numText);
	}

	// Following method reads the xml file and display its content 
	public void readXml(int numText){

		foreach(XmlElement node in xmlDoc.SelectNodes("conversaciones/conversacion")){
			fileDataTextbox.text = "";
			fileDataTextboxDown.text = "";
			
			string textoConversacionTemp = node.SelectSingleNode("texto").InnerText;
			listaConversaciones.Add(textoConversacionTemp);
		}
		
		StartCoroutine ("generaFraseAnimada",numText);
		
		//Acaba el parse conversaciones
		
		//fileDataTextbox.text = listaConversaciones[0];// /t->espacio /n->intro
	}
	
	IEnumerator generaFraseAnimada(int numConversacion){
		char [] caracteres = listaConversaciones [numConversacion].ToCharArray();

		//Debug.Log (caracteres [0]);
		foreach (char character in caracteres) {
			letras.Play();

			fileDataTextbox.text +=  character.ToString();
			fileDataTextboxDown.text += character.ToString();
			yield return new WaitForSeconds(0.05f);
		}
		
		//fileDataTextbox.text = listaConversaciones[0];
	}

	//	private void displayPlayerData(Player tempPlayer){
	//		fileDataTextbox.text += tempPlayer.fecha + "\t\t" + tempPlayer.name + "\t\t\t" + tempPlayer.score + "\n";// /t->espacio /n->intro
	//	}
}