﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using UnityEngine.UI;

public class XMLConversationParsing : MonoBehaviour {

	public Text fileDataTextboxUp;
	public Text fileDataTextboxDown;
	private XmlDocument xmlDoc;
	public TextAsset conversaciones;
	
	private List<Conversacion> listaConversaciones;
	
	// Structure for mainitaing the player information
	struct Conversacion{
		public string id;
		public string texto;
	};
	
	void Start(){
		xmlDoc = new XmlDocument();

		listaConversaciones = new List<Conversacion> ();

		xmlDoc.LoadXml (conversaciones.text);
		
		//readXml(4);
	}
	
	// Following method reads the xml file and display its content 
	public void readXml(int numText){

		foreach(XmlElement node in xmlDoc.SelectNodes("conversaciones/conversacion")){
			fileDataTextboxUp.text = "";
			fileDataTextboxDown.text = "";

			Conversacion tempConversacion = new Conversacion();

			tempConversacion.id = node.SelectSingleNode("id").InnerText;
			tempConversacion.texto = node.SelectSingleNode("texto").InnerText;

			listaConversaciones.Add(tempConversacion);
			displayConversationData(tempConversacion);
		}
		
		StartCoroutine (generaFraseAnimada(numText));
		
		//Acaba el parse conversaciones
		
		//fileDataTextboxUp.text = listaConversaciones[0];// /t->espacio /n->intro
	}
	
	IEnumerator generaFraseAnimada(int numConversacion){
		char [] caracterestexto = listaConversaciones [numConversacion].texto.ToCharArray();
		
		//Debug.Log (caracteres [0]);
		foreach (char character in caracterestexto) {
			fileDataTextboxUp.text +=  character.ToString();
			fileDataTextboxDown.text +=  character.ToString();
			yield return new WaitForSeconds(0.05f);
		}
		
		//fileDataTextboxUp.text = listaConversaciones[0];
	}

	private void displayConversationData(Conversacion tempConversacion){
		fileDataTextboxUp.text += tempConversacion.id + "\n" + "\n";
		fileDataTextboxDown.text += tempConversacion.id + "\n"+ "\n";
	}
}