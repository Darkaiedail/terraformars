﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ActivaPanel : MonoBehaviour {

	//variables para sacar los textos en pantalla
	public GameObject panelText;
	public float[] tiempoTexto;
	public Text nombrePj;

	//variables para las animaciones de hablar de los personajes
	private GameObject comDavis;
	private GameObject nanao;

	void Update(){
		comDavis = GameObject.FindGameObjectWithTag ("Comandante");
		nanao = GameObject.FindGameObjectWithTag ("Nanao");
	}

	void activaTexto(){
		panelText.SetActive (true);
		nombrePj.text = "Nanao Akita";
		nanao.SendMessage("talkNanao");
		StartCoroutine (activaSiguienteTexto());
	}
	
	IEnumerator activaSiguienteTexto(){
		yield return new WaitForSeconds (tiempoTexto[0]);
		nombrePj.text = "Comandante Davis";
		nanao.SendMessage("stopTalkNanao");
		comDavis.SendMessage ("talk");
		StartCoroutine (desactivaTexto());
	}

	IEnumerator desactivaTexto(){
		yield return new WaitForSeconds (tiempoTexto[1]);
		panelText.SetActive (false);
		comDavis.SendMessage ("stopTalk");
	}
}
