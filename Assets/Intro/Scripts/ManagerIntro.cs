﻿using UnityEngine;
using System.Collections;

public class ManagerIntro : MonoBehaviour {

	private GameObject canvas;

	public AudioSource button;
	
	void Start(){
		canvas = GameObject.FindGameObjectWithTag("Canvas");
	}

	void Update () {
		if (Input.GetKey (KeyCode.Escape)) {
			button.Play();
			canvas.SendMessage("FadeScape");
		}
	}
}
