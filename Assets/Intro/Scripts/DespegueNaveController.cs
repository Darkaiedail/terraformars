﻿using UnityEngine;
using System.Collections;

public class DespegueNaveController : MonoBehaviour {

	public float tiempo;
	public GameObject planetMars;

	//sonidos
	public AudioSource nave;

	void Start () {
		nave.Play ();

		iTween.MoveTo (this.gameObject, iTween.Hash ("x", 28.593f,
		                                             "y", 12.74f,
		                                             "time", tiempo,
		                                             "easetype","linear",
		                                             "oncomplete","rotate"));

	}

	IEnumerator sonidoNave(){
		yield return new WaitForSeconds (2.0f);
		nave.Play ();
	}

	void rotate(){
		iTween.RotateTo (this.gameObject, iTween.Hash ("z", 0.0f,
		                                               "time", 2.0f));
		planetMars.SendMessage("acercar");
	}
}
