﻿using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour {

	private Animator animator;

	void Awake(){
		animator = GetComponent<Animator> ();
	}

	//para hacer un fundido a negro al final de fase
	void FadeIn(){
		StartCoroutine (fade ());
	}

	IEnumerator fade(){
		yield return new WaitForSeconds (10.0f);
		animator.Play (Animator.StringToHash ("FadeInOut"));
		StartCoroutine (loadLevel(1.5f));
	}

	//Para hacer un fundido a negro si das a esc
	void FadeScape(){
		StartCoroutine (fadeSc ());
	}
	
	IEnumerator fadeSc(){
		yield return new WaitForSeconds (0.05f);
		animator.Play (Animator.StringToHash ("FadeInOut"));
		StartCoroutine (loadLevel(1.0f));
	}

	IEnumerator loadLevel(float time){
		yield return new WaitForSeconds (time);
		Application.LoadLevel (5);
	}
}